from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from security.authentication_filter import SafeJWTAuthentication
from .models import Enquiry
from .serializers import EnqiurySerializer, EnqiuryListingSerializer


class EnquiryView(APIView):
    @staticmethod
    def post(request):
        try:

            serializer = EnqiurySerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({"message": "Inquiry created successfully"}, status.HTTP_201_CREATED)
            else:
                return Response({"message": "Request data not proper format.", },
                                status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            return Response({"message": "Something went wrong."}, status.HTTP_500_INTERNAL_SERVER_ERROR)


class EnquiryListing(APIView):
    @staticmethod
    def get(request):
        try:
            enquiries = Enquiry.objects.filter(claim=None)
            serializer = EnqiuryListingSerializer(enquiries, many=True)
            return Response(serializer.data, status.HTTP_200_OK)
        except Exception as e:
            return Response({"message": "Something went wrong."}, status.HTTP_500_INTERNAL_SERVER_ERROR)


class ClaimEnquiryView(APIView):
    authentication_classes = [SafeJWTAuthentication]

    @staticmethod
    def post(request):
        try:
            enquiry = Enquiry.objects.get(id=request.data['enquiry_id'], claim=None)
            enquiry.claim = request.user
            enquiry.save()
            return Response({"message": "inquiry claimed successfully."})
        except Enquiry.DoesNotExist as e:
            return Response({"message": "Enquiry not exist."}, status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response({"message": "Something went wrong."}, status.HTTP_500_INTERNAL_SERVER_ERROR)
