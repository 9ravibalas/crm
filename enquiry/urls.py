from django.urls import path

from .views import EnquiryView, EnquiryListing, ClaimEnquiryView

urlpatterns = [
    path('', EnquiryListing.as_view()),
    path('create', EnquiryView.as_view()),
    path('claim', ClaimEnquiryView.as_view())

]
