from django.db import models

from user.models import User


class Enquiry(models.Model):
    class Meta:
        db_table = 'enquires'

    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    email = models.EmailField()
    course_interest = models.CharField(max_length=100)
    claim = models.ForeignKey(User, on_delete=models.CASCADE, null=True)  # user who claim this inquiry
