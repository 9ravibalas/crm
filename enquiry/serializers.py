from rest_framework import serializers

from .models import Enquiry


class EnqiurySerializer(serializers.ModelSerializer):
    class Meta:
        model = Enquiry
        fields = "__all__"


class EnqiuryListingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Enquiry
        fields = ["id", "email", "name", "surname", "course_interest"]
