import json

from django.conf import settings
from jwcrypto import jwt, jwk
from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import NotFound, AuthenticationFailed

from user.models import User

key = jwk.JWK(**settings.SECRET_KEY_JWT)


def token_decode(request):
    try:
        authorization_header = request.headers.get('Authorization')
        access_token = authorization_header.split(' ')[1]
        access_token = jwt.JWT(key=key, jwt=access_token)
        access_token = jwt.JWT(key=key, jwt=access_token.claims)
        claims = json.loads(access_token.claims)
        user = User.objects.get(email=claims['email'])
        return user, claims
    except User.DoesNotExist as e:
        raise NotFound(detail="token not valid.")
    except Exception as e:
        raise AuthenticationFailed(detail="token not valid.")


class SafeJWTAuthentication(BaseAuthentication):
    def authenticate(self, request):
        client = token_decode(request)
        return client
