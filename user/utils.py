import time

from django.conf import settings
from jwcrypto import jwk, jwt

key = jwk.JWK(**settings.SECRET_KEY_JWT)


def generate_token(user):
    access_token = jwt.JWT(header={"alg": settings.JWT_ALG, "type": "access"},
                           claims={"email": user.email, "login_time": time.time(), },
                           default_claims={"exp": round(time.time()) + settings.ACCESS_VALID_SECONDS})

    access_token.make_signed_token(key)
    access_token = access_token.serialize()
    encrypted_access_token = jwt.JWT(header=settings.ENCRYPT_ALG_DATA, claims=access_token)
    encrypted_access_token.make_encrypted_token(key)
    encrypted_access_token = encrypted_access_token.serialize()

    return encrypted_access_token
