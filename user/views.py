from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from user.models import User
from user.serializers import UserSerializer, LoginSerializer
from .utils import generate_token


class Registration(APIView):
    @staticmethod
    def post(request):
        try:

            serializer = UserSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({"message": "User registration successfully."}, status.HTTP_201_CREATED)
            else:
                return Response({"message": "Request data not proper format."}, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            return Response({"message": "Something went wrong."}, status.HTTP_500_INTERNAL_SERVER_ERROR)


class Login(APIView):
    @staticmethod
    def post(request):
        try:

            serializer = LoginSerializer(data=request.data)
            if serializer.is_valid():
                user = User.objects.get(**serializer.data)
                return Response({"token": generate_token(user)}, status.HTTP_200_OK)
            else:
                return Response({"message": "Request data not proper format."}, status=status.HTTP_400_BAD_REQUEST)
        except User.DoesNotExist as e:
            return Response({"message": "wrong email or password."}, status.HTTP_403_FORBIDDEN)
        except Exception as e:
            return Response({"message": "Something went wrong."}, status.HTTP_500_INTERNAL_SERVER_ERROR)
