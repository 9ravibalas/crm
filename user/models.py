from django.db import models


class User(models.Model):
    class Meta:
        db_table = 'user'

    email = models.EmailField(unique=True)
    password = models.TextField()
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)

    def __str__(self):
        return " ".join([self.name, self.surname])
